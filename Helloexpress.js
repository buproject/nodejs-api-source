const express = require('express');
const app = express();
const port = 3000
const bodyParser = require('body-parser');
app.use(bodyParser.json());


app.get('/', (req, res) => res.send('Hello World!'))

app.post('/', (req, res) => res.json({
    success: true,
    asd: req.body,
}))


app.listen(port, () => console.log('Express js app listening on port:' + port))