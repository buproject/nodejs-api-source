  var http=require("http");

  var hostname = '127.0.0.1';
  var port = '3000';

  var server = http.createServer(function(request,response){
    response.writeHead(200 ,{"Content-Type":"text/plain"});
    response.end('Hello World\n');
});

  server.listen(port, hostname, function() {
    console.log("Listening on "+port);
    });

      console.log('Server running at http://'+hostname+':'+port);
